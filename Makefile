run:
	docker-compose up --build -d nginx
	@sleep 10
	@docker-compose exec web-back bash -c "./manage.py migrate"
	@docker-compose exec web-back bash -c "./manage.py create_super_user"
	@docker-compose exec web-back bash -c "./manage.py create_super_user --username admin2 --email a2@admin.com --password admin2"
	@docker-compose exec web-back bash -c "./manage.py collectstatic --noinput"
	@echo "http://0.0.0.0/admin/ - admin:admin"
