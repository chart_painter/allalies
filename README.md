# Run

* Run the script:
```bash
git clone --remote-submodules --recurse-submodules -j8 git@gitlab.com:chart_painter/allalies.git app
cd app
make run
```
* Open [admin panel](http://0.0.0.0/admin/) and login via admin:admin
